# Karat interview 2

Karat interview redo. I guess I don't perform all that well in a video
skills test environment (aka 'gun to head'). Something to work on...

<ins>Task</ins>

You are running a classroom and suspect that some of your students
are passing around the answers to multiple choice questions
disguised as random strings.

Your task is to write a function that, given a list of words and a
string, finds and returns the word in the list that is scrambled up
inside the string, if any exists. There will be at most one matching
word. The letters don't need to be in order or next to each
other. The letters cannot be reused.
