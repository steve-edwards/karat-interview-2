#!/bin/perl

# You are running a classroom and suspect that some of your students
# are passing around the answers to multiple choice questions
# disguised as random strings.

# Your task is to write a function that, given a list of words and a
# string, finds and returns the word in the list that is scrambled up
# inside the string, if any exists. There will be at most one matching
# word. The letters don't need to be in order or next to each
# other. The letters cannot be reused.

# Example:
# words = ['cat', 'baby', 'dog', 'bird', 'car', 'ax']
# string1 = 'tcabnihjs'
# find_embedded_word(words, string1) -> cat (the letters do not have to be in order)

# string2 = 'tbcanihjs'
# find_embedded_word(words, string2) -> cat (the letters do not have to be together)

# string3 = 'baykkjl'
# find_embedded_word(words, string3) -> None / null (the letters cannot be reused)

# string4 = 'bbabylkkj'
# find_embedded_word(words, string4) -> baby

# string5 = 'ccc'
# find_embedded_word(words, string5) -> None / null

# string6 = 'breadmaking'
# find_embedded_word(words, string6) -> bird

# Complexity analysis variables:

# W = number of words in `words`
# S = maximal length of each word or string

use	strict;
use	warnings;
use     Data::Dumper qw(Dumper);

use	constant			nl	=> "\n";	# newline

my @words = qw(cat baby dog bird car ax);
my $string1 = "tcabnihjs";
my $string2 = "tbcanihjs";
my $string3 = "baykkjl";
my $string4 = "bbabylkkj";
my $string5 = "ccc";
my $string6 = "breadmaking";

# this was 'so close' -- sad trombone
sub					x_find_embedded_word()
	{
	my ($answers, $jumble) = @_;

	foreach my $original_answer (@$answers)
		{
		my $answer = $original_answer;
		my $statement = "tr/$jumble//d;";
		eval('$answer =~ ' . $statement);
		if	(!$answer)
			{
			return($original_answer);
			}
		}

	return('** none **');
	}

# this works, but it seems a bit clunky
sub					find_embedded_word()
	{
	my ($answers, $original_jumble) = @_;
	foreach my $answer (@$answers)
		{
		my $found = 1;
		my $jumble = $original_jumble;
#print 'Is ' . $answer . ' in ' . $jumble . nl;
		foreach	my $character (split(//, $answer))
			{
#print 'Is ' . $character . ' in ' . $jumble . nl;
			my $idx = index($jumble, $character);
#$DB::single = 1;
			if	(-1 == $idx)
				{
				$found = 0;
				last;
				}
			$jumble = substr($jumble, 0, $idx)
				. substr($jumble, $idx + 1);
			}
#$DB::single = 1;
		if	($found)
			{
			return($answer);
			}
		}

	return('** none **');
	}

print 'Answer found in ' . $string1 . ' = ' . &find_embedded_word(\@words, $string1) . nl;
print 'Answer found in ' . $string2 . ' = ' . &find_embedded_word(\@words, $string2) . nl;
print 'Answer found in ' . $string3 . ' = ' . &find_embedded_word(\@words, $string3) . nl;
print 'Answer found in ' . $string4 . ' = ' . &find_embedded_word(\@words, $string4) . nl;
print 'Answer found in ' . $string5 . ' = ' . &find_embedded_word(\@words, $string5) . nl;
print 'Answer found in ' . $string6 . ' = ' . &find_embedded_word(\@words, $string6) . nl;
